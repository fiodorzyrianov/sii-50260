#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main()
{
	char fifo[]="/home/fiodor/50260/sii-50260/practica1/build/fifo1";
	int fd;
	char buf[10];
	
	if((mkfifo(fifo, 0777))<0)
	{
		perror("Error al crear la tuberia");
		exit(1);
	}
	fd=open(fifo,O_RDONLY);
	
	for(;;)
	{
		read(fd,buf,sizeof(buf));
		printf("Jugador%c ha marcado %c puntos\n",buf[1],buf[2]);
		if(buf[0]=='1')
		{
			printf("Jugador %c GANA!!!\n",buf[1]);
			break;
		}
	}
	close(fd);
	unlink(fifo);
}
